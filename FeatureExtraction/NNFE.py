# -*- coding: utf-8 -*-
"""
Created on Tue Oct  4 18:11:50 2016

@author: xiaolin
"""
""" 
 Nonlinear mapping using neural network, in this version, particle swarm optimization is used
"""
import numpy as np
import random
from pyswarm import pso
random.seed( 99 )

class NNFE(object):
    def __init__(self, X, y, parameters):
        #Input data
        self.X=X
        #Output data
        self.y=y
        #Expect parameters to be a tuple of the form:
        #    ((n_input,0,0), (n_hidden_layer_1, f_1, f_1'), ...,
        #     (n_hidden_layer_k, f_k, f_k'), (n_output, f_o, f_o'))
        self.n_layers = len(parameters)
        #Counts number of neurons without bias neurons in each layer.
        self.sizes = [layer[0] for layer in parameters]
        #Activation functions for each layer.
        self.fs =[layer[1] for layer in parameters]
        #Derivatives of activation functions for each layer.
        self.fgrads = [layer[2] for layer in parameters]
        self.build_network()
 
    def build_network(self):
        #List of weight matrices taking the output of one layer to the input of the next.
        self.weights=[]
        #Bias vector for each layer.
        self.biases=[]
        #Input vector for each layer.
        self.inputs=[]
        #Output vector for each layer.
        self.outputs=[]
        #Vector of delta at each layer.
        self.delta=[]
        #We initialise the weights randomly, and fill the other vectors with 1s.
        for layer in range(self.n_layers-1):
            n = self.sizes[layer]
            m = self.sizes[layer+1]
            self.weights.append(np.random.normal(0,1, (m,n)))
            self.biases.append(np.random.normal(0,1,(m,1)))
            self.inputs.append(np.zeros((n,1)))
            self.outputs.append(np.zeros((n,1)))
            self.delta.append(np.zeros((n,1)))
        #There are only n-1 weight matrices, so we do the last case separately.
        n = self.sizes[-1]
        self.inputs.append(np.zeros((n,1)))
        self.outputs.append(np.zeros((n,1)))
        self.delta.append(np.zeros((n,1)))
 
    def feedforward(self, x):
        #Propagates the input from the input layer to the output layer.
        #k=len(x)
        #x.shape=(k,1)
        self.inputs[0]=x
        self.outputs[0]=x
        for i in range(1,self.n_layers):
            self.inputs[i]=self.weights[i-1].dot(self.outputs[i-1])+self.biases[i-1]
            self.outputs[i]=self.fs[i](self.inputs[i])
        return self.outputs[-1]
 
   
    def predict_x(self, x):
        return self.feedforward(x)
 
    def predict(self, X):
        n = len(X)
        m = self.sizes[-1]
        results = np.ones((n,m))
        for i in range(len(X)):
            results[i,:] = self.feedforward(X[i:i+1,:].T).reshape((m))
        return results

    def predict_output(self,w):
        pre_w = 0     
        for layer in range(self.n_layers-1):
            n = self.sizes[layer]
            m = self.sizes[layer+1]
            self.weights[layer] =  w[pre_w:(pre_w + m*n)].reshape(m,n)            
            self.biases[layer] = w[(pre_w + m*n):(pre_w + m*n + m)].reshape(m,1)
            pre_w = pre_w + m*n + m
            
        output = self.predict(self.X)
        return output        
    
 
    def train(self):
        
        lb = [-1]*( np.size(self.weights[0]) + np.size(self.weights[1])  + np.size(self.biases[0]) +np.size(self.biases[1]))
        ub = [1]*( np.size(self.weights[0]) + np.size(self.weights[1])  + np.size(self.biases[0]) +np.size(self.biases[1]))
        xopt, fopt = pso(self._func, lb, ub, maxiter=50, minfunc=1e-8, debug = True)
        w = xopt
        # get the final output
        pre_w = 0     
        for layer in range(self.n_layers-1):
            n = self.sizes[layer]
            m = self.sizes[layer+1]
            self.weights[layer] =  w[pre_w:(pre_w + m*n)].reshape(m,n)            
            self.biases[layer] = w[(pre_w + m*n):(pre_w + m*n + m)].reshape(m,1)
            pre_w = pre_w + m*n + m
        output = self.predict(self.X)
        
        return output, -fopt
    
    def _func(self, w):
        pre_w = 0     
        for layer in range(self.n_layers-1):
            n = self.sizes[layer]
            m = self.sizes[layer+1]
            self.weights[layer] =  w[pre_w:(pre_w + m*n)].reshape(m,n)            
            self.biases[layer] = w[(pre_w + m*n):(pre_w + m*n + m)].reshape(m,1)
            pre_w = pre_w + m*n + m
            
        output = self.predict(self.X)
        XY = np.hstack((output,self.y))
        
        return -self._func_MI_Reg(XY)
 
    
    def _func_MI_Reg(self, XY, split = [1,1]):
        """ kernel estimation of mutual information, only works for two dimensional problems. The second column is from a continous variable
    
        Parameters
        ----------
        XY: array,  [n_samples, 2]
            each column represent a variable
    
        Returns
        -------
        return the mutual information between X and Y
    
        """                        
        n_samples = XY.shape[0]
        proj_x = XY[:,0]
        proj_y = XY[:,1]
        std_1 = np.std(proj_x)
        std_2 = np.std(proj_y)
        h_1 = 1.06*std_1*n_samples**(-0.2)
        #print 1.06*std_1*n_samples**(-0.2)
        h_2 = 1.06*std_2*n_samples**(-0.2)
        #print 1.06*std_2*n_samples**(-0.2)
        h_11 = std_1*n_samples**(-1./6)
        #print std_1*n_samples**(-1./6)
        h_22 = std_2*n_samples**(-1./6)  
        #print std_2*n_samples**(-1./6)  
        sum_mi = 0
        for i in range(n_samples):
            sum_mi = sum_mi + np.log(self._sumJointGKernel(proj_x[i], proj_x, proj_y[i], proj_y, h_11, h_22)) - \
            np.log(self._sumSingleGKernel(proj_x[i], proj_x.reshape(-1,1),h_1)) - \
            np.log(self._sumSingleGKernel(proj_y[i], proj_y.reshape(-1,1),h_2))
              
        return sum_mi/n_samples
    
    def _GKernel(self,x):
        
        """ Gaussian kernel
    
        Parameters
        ----------
        x: scalar or vector
    
        Returns
        -------
        return corresponding scalar or vector
    
        """ 
        return 1./np.sqrt(2*np.pi)*np.exp(-0.5*(x.astype(float)**2))
        
        
    def _sumSingleGKernel(self,x, x_vec,h):
        
        """ kernel density estimation for single variable
    
        Parameters
        ----------
        x: float
            point that need to be evaluated
        x_vec: vector, (, n_samples)
            samples that construct the kernel estimator
        h: float
            bandwidth of kernel estimator
    
        Returns
        -------
        return the density of x
    
        """         
        sumGK = 1./(len(x_vec)*h)*np.sum(self._GKernel((x - x_vec)/h))
        return sumGK
        
        
    def _sumJointGKernel(self,x, x_vec, y, y_vec, h_11, h_22):
        
        """ kernel density estimation for joint variable
    
        Parameters
        ----------
        x: float
            (x,y) is the point that need to be evaluated
        x_vec: vector, (, n_samples)
            (x_vec, y_vec) are samples that construct the kernel estimator
        y: float
            (x,y) is the point that need to be evaluated
        y_vec: vector, (, n_samples)
            (x_vec, y_vec) are samples that construct the kernel estimator
        h_11: float
            bandwidth of kernel estimator for x
        h_22: float
            bandwidth of kernel estimator for y
        
        Returns
        -------
        return the mutual information between X and Y
        
        """ 
        sumGK = 1./(len(x_vec)*h_11*h_22)*np.sum(self._GKernel((x - x_vec)/h_11)*self._GKernel((y - y_vec)/h_22))
        return sumGK
    
    
    
    
    
    
    
    
    
    
    
    
