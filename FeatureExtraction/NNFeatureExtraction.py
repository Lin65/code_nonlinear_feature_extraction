from scipy.optimize import minimize
import copy
import numpy as np
from scipy.optimize import differential_evolution
from pyswarm import pso
#from sklearn.neighbors.kde import KernelDensity
import time
from NNFE import NNFE

np.random.seed(seed=99)
class NFE(object):
    """Nonlinear feature extraction (NFE)
    
    Supervised nonlinear dimensionality reduction by selecting nonlinear projections that maximize the dependence
    on the target at the same time minimize the dependence within projections. Mutual information is
    used as a metric for dependence, and is estimated via kernel estimation. 
    
    s = arg max I(Y;s) - a \sum I(s, s_i)  
    

    Parameters
    ----------
    n_components: int or string
        Number of components to keep.
        if n_components == 'min_dependency', process will end once \sum I(s, s_i) >= b. Default value is 'min_dependency'
        
    b: float or string
        The maximum inner pairwise dependence that can be torlerated.  b is used only if n_components == 'min_dependency'.Defaul value is 0.01 
        
    a: float or string 
        The coefficient of the regularizer. Defaul value is 1.
        if a == 'average', a = 1/Ns, Ns is the number of current extracted features
        
    optimizer: string, optional
        Default value is 'PSO'. Other optimization methods from scipy can also be used.
    
    task: string {'regression', 'classification'}
        indicate whether it is a regression problem or classification. Default is 'regression'
        
    Attributes
    ----------
    components_ : array, [n_components, n_weights]
        weights of neural network
        
    projections_: array, [n_samples, n_components]
        output of neural network        
        
    mi_: vector, (,n_components)
        record mutual information between Y and each projection.
    
    n_components: int
        The estimated number of components. Relevant when n_components is set to 'min_dependency'.
        
    
    Examples
    --------
    
    >>> import numpy as np
    >>> import SFE
    >>> X = np.array([[],[],[])
    >>> Y = np.array([])
    >>> sfe = NFE(n_components='min_dependency', a=1, b= 0.1, type = 'regression')
    >>> sfe.fit(X,Y)
    >>> sfe.n_components
    >>> sfe.components_
    >>> sfe.mi_
    
    """
    
    
    def __init__(self, n_components='min_dependency', a = 1, b = 0.01, optimizer = 'PSO', task ='regression' ):
        self.n_components = n_components
        self.optimizer = optimizer
        self.a = a
        self.b = b
        self.type = task
 
    def fit(self, X, Y, param):
        """Fit the model with X,Y.
    
        Parameters
        ----------
        X: array-like, shape (n_samples, n_features)
            Training data, where n_samples is the number of samples and n_features is the number of features.
        Y: vector, shape (1, n_samples)    
    
        Returns
        -------
        self : object
            Return the instance itself
        """
        self.param = param
        self.n_weights = self._getNumOfWeights()
        self._fit(X,Y)
        return self
        
    def _getNumOfWeights(self):
        
        self.n_layers = len(self.param)
        self.sizes = [layer[0] for layer in self.param]
        
        num_weights = 0
        
        for layer in range(self.n_layers-1):
            n = self.sizes[layer]
            m = self.sizes[layer+1]
            num_weights = num_weights + m*n
            num_weights = num_weights + m
        
        return num_weights
  
    def _fit(self, X,Y):
        
        """Fit the model with X and Y, self.components_ is calculated
    
        Parameters
        ----------
        X: array-like, shape (n_samples, n_features)
            Training data, where n_samples is the number of samples and n_features is the number of features.
        Y: vector, shape ( , n_samples)    
    
        Returns
        -------
        No return value
    
        """ 
        self.X = X
        self.Y = Y
        n_samples, n_features = X.shape
        
        # initialization
        n_TBD = False 
        if self.n_components == 'min_dependency':
            self.n_components = n_features
            n_TBD = True
            
        self.components_ = np.zeros((self.n_components, self.n_weights))
        self.projections_ = np.zeros((n_samples,self.n_components))
        self.mi_ = np.zeros(self.n_components)
        
        # restrict the weight of direction in ( -1, 1)
        self.bounds_ = [(-1,1)]*self.n_weights
        for i in range(0, self.n_components):
            
            print 'Processing component %d'%i
            # calculate time elapse for each component
            start_time = time.time()
            
            self.current_component_ = i
            if self.optimizer == 'PSO':
                lb = [-1.]*self.n_weights
                ub = [1.]*self.n_weights
                xopt, fopt = pso(self._func, lb, ub,swarmsize =50, maxiter=500, minstep = 1e-8, minfunc=1e-8,debug = False)
                weights = xopt
                output = self._neuralNetwork(weights)
            
            end_time = time.time()
            time_elapse = end_time - start_time
            print 'time elapse for component %d is %d s'%(i,time_elapse)
            self.components_[i,:] = weights.T
            self.projections_[:,i:(i+1)] = output 
            projXY = np.hstack((output, Y.reshape(-1,1)))
            if self.type =='regression':
                self.mi_[i] = self._func_MI_Reg(projXY, [1,1]) 
            elif self.type == 'classification':
                self.mi_[i] = self._func_MI_Cla(projXY,[1,1])
            print 'mutual information: ',  self.mi_[i]  
            
            # inner dependency
            print 'mutual information between the new projection and previous projections are: '
            for j in range(0, self.current_component_):
                projXX = np.vstack((self.projections_[:,self.current_component_], self.projections_[:,j])).T
                print self._func_MI_Reg(projXX,[1,1])
            
            if (n_TBD == True) and (self.current_component_>0):
                # we need to calculate inner denpendency and stop the process if it is bigger than self.b
                for j in range(0, self.current_component_):
                    projXX = np.vstack((self.projections_[:,self.current_component_], self.projections_[:,j])).T
                    if self._func_MI_Reg(projXX,[1,1]) >= self.b: #0.2*self.mi_[self.current_component_]:
                        self.n_components = self.current_component_
                        self.components_ = self.components_[0:self.n_components, 0:n_features]
                        self.projections_ = self.projections_[0:n_samples, 0:self.n_components]
                        self.mi_ = self.mi_[:self.n_components]
                        return
                        
    def _neuralNetwork(self, weights):
        param = self.param
        nnfe=NNFE(self.X, self.Y.reshape(-1,1),param)
        output = nnfe.predict_output(weights)
        del nnfe
        return output
        
        
        
    def _func(self,x):
        
        """ calculate the cost function
    
        Parameters
        ----------
        x: a vector with shape ( ,n_features)
            represent the projecting direction       
    
        Returns
        -------
        return the value of the cost function
    
        """ 
        weights = x
        #------
        projX = self._neuralNetwork(weights)
        #-------
        projXY = np.hstack((projX, self.Y.reshape(-1,1)))
        
        if self.type == 'regression':
            if(self.current_component_ == 0):
                indep_constraints = 0
            else:
                indep_constraints = max(self._indep_fun(x),0.)
                if self.a == 'average':
                    return -self._func_MI_Reg(projXY,[1,1]) + 1./self.current_component_ *indep_constraints
                else:
                    return -self._func_MI_Reg(projXY,[1,1]) + self.a *indep_constraints
        
            return -self._func_MI_Reg(projXY, [1,1])    
        
        elif self.type == 'classification':
            if(self.current_component_ == 0):
                indep_constraints = 0
            else:
                indep_constraints = max(self._indep_fun(x),0.)
                if self.a == 'average':
                    return -self._func_MI_Cla(projXY,[1,1]) + 1/self.current_component_ *indep_constraints
                else:
                    return -self._func_MI_Cla(projXY,[1,1]) + self.a *indep_constraints
        
            return -self._func_MI_Cla(projXY, [1,1]) 
            
            
    def _indep_fun(self, x):
        
        """ lagrange multiplier (new projection should hava 0 mutual information with previous ones)
    
        Parameters
        ----------
        x: a vector with shape ( ,n_features)
            represent the projecting direction        
    
        Returns
        -------
        return the value of the cost function
    
        """ 
        weights = x
        #------
        projX = self._neuralNetwork(weights)
        #-------
        reg = 0.
        for i in range(0, self.current_component_):
            projXX = np.hstack((self.projections_[:,i].reshape(-1,1), projX))
            reg = reg + max(self._func_MI_Reg(projXX,[1,1]),0)
        return reg
        

    def _func_MI_Reg(self, XY, split = [1,1]):
        """ kernel estimation of mutual information, only works for two dimensional problems. The second column is from a continous variable
    
        Parameters
        ----------
        XY: array,  [n_samples, 2]
            each column represent a variable
    
        Returns
        -------
        return the mutual information between X and Y
    
        """         
        
        
        n_samples = XY.shape[0]
        proj_x = XY[:,0]
        proj_y = XY[:,1]
        std_1 = np.std(proj_x)
        std_2 = np.std(proj_y)
        h_1 = 1.06*std_1*n_samples**(-0.2)
        #print 1.06*std_1*n_samples**(-0.2)
        h_2 = 1.06*std_2*n_samples**(-0.2)
        #print 1.06*std_2*n_samples**(-0.2)
        h_11 = std_1*n_samples**(-1./6)
        #print std_1*n_samples**(-1./6)
        h_22 = std_2*n_samples**(-1./6)  
        #print std_2*n_samples**(-1./6)
        sum_mi = 0
        for i in range(n_samples):
            sum_mi = sum_mi + np.log(self._sumJointGKernel(proj_x[i], proj_x, proj_y[i], proj_y, h_11, h_22)) - \
            np.log(self._sumSingleGKernel(proj_x[i], proj_x.reshape(-1,1),h_1)) - \
            np.log(self._sumSingleGKernel(proj_y[i], proj_y.reshape(-1,1),h_2))
              
        return sum_mi/n_samples
  
            
    def _func_MI_Cla(self, XY, split = [1,1]):
        """ kernel estimation of mutual information, only works for two dimensional problems.The second column is from a discrete variable
    
        Parameters
        ----------
        XY: array,  [n_samples, 2]
            each column represent a variable
    
        Returns
        -------
        return the mutual information between X and Y
    
        """         

        n_samples = XY.shape[0]
        proj_x = XY[:,0]
        proj_y = XY[:,1]
        std_1 = np.std(proj_x)
        h_1 = 1.06*std_1*n_samples**(-0.2)
            
        # calculate H(proj_x)
        Ent_x = 0
        for i in range(0,n_samples):
            Ent_x = Ent_x + np.log(self._sumSingleGKernel(proj_x[i], proj_x.reshape(-1,1),h_1))
        Ent_x = - Ent_x/n_samples
            
        # calculate \sum H(proj_x|yi)
        sum_Ent = 0
        n_classes = np.unique(proj_y)
        for yi in n_classes:
            indices = np.where(proj_y == yi)[0]
            n_samples_sub = len(indices)
            # frequency of yi
            prop = n_samples_sub/float(n_samples)
            proj_x_sub = proj_x[indices]  
            std_sub = np.std(proj_x_sub)
            h_sub = 1.06*std_sub*n_samples_sub**(-0.2)
            if h_sub == 0.0:
                Ent_sub = 0.
                
            else:
                Ent_sub = 0
                # calculate H(proj_x|yi)
                for i in range(0,n_samples_sub):
                    Ent_sub = Ent_sub + np.log(self._sumSingleGKernel(proj_x_sub[i], proj_x_sub.reshape(-1,1),h_sub))
                
                Ent_sub = - Ent_sub/n_samples_sub
            sum_Ent = sum_Ent + prop*Ent_sub
            
        return Ent_x - sum_Ent   
            

    def _GKernel(self,x):
        
        """ Gaussian kernel
    
        Parameters
        ----------
        x: scalar or vector
    
        Returns
        -------
        return corresponding scalar or vector
    
        """ 
        return 1./np.sqrt(2*np.pi)*np.exp(-0.5*(x.astype(float)**2))
        
        
    def _sumSingleGKernel(self,x, x_vec,h):
        
        """ kernel density estimation for single variable
    
        Parameters
        ----------
        x: float
            point that need to be evaluated
        x_vec: vector, (, n_samples)
            samples that construct the kernel estimator
        h: float
            bandwidth of kernel estimator
    
        Returns
        -------
        return the density of x
    
        """ 
        if h==0.:        
            print h
        sumGK = 1./(len(x_vec)*h)*np.sum(self._GKernel((x - x_vec)/h))
        return sumGK
        
        
    def _sumJointGKernel(self,x, x_vec, y, y_vec, h_11, h_22):
        
        """ kernel density estimation for joint variable
    
        Parameters
        ----------
        x: float
            (x,y) is the point that need to be evaluated
        x_vec: vector, (, n_samples)
            (x_vec, y_vec) are samples that construct the kernel estimator
        y: float
            (x,y) is the point that need to be evaluated
        y_vec: vector, (, n_samples)
            (x_vec, y_vec) are samples that construct the kernel estimator
        h_11: float
            bandwidth of kernel estimator for x
        h_22: float
            bandwidth of kernel estimator for y
        
        Returns
        -------
        return the mutual information between X and Y
        
        """ 
        sumGK = 1./(len(x_vec)*h_11*h_22)*np.sum(self._GKernel((x - x_vec)/h_11)*self._GKernel((y - y_vec)/h_22))
        return sumGK

    def transform(self,X_new):
        
        """ project new data on obtained directions
        
        Parameters
        ----------
        X_new: array, has the same number of features as self.X
        
        Returns
        -------
        return the projections of X_new
        
        """
        param = self.param
        nnfe=NNFE(X_new, self.Y.reshape(-1,1),param)
        num_smps = X_new.shape[0]
        output = np.zeros((num_smps, self.n_components))
        for i in range(self.n_components):       
            weights = self.components_[i,:]
            output[:,i:(i+1)] = nnfe.predict_output(weights)
            
        return output


