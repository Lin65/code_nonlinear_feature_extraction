# -*- coding: utf-8 -*-
"""
Created on Tue Oct  4 23:01:24 2016

@author: xiaolin
"""
import numpy as np
from NNFE import NNFE
from scipy.special import expit
import matplotlib.pyplot as plt

def tanh(x):
    return np.tanh(x)
def tanh_grad(x):
    return 1.0 - np.tanh(x)**2.0

def logistic(x):
    return 1.0/(1+np.exp(-x))
 
def logistic_grad(x):
    ex=expit(-x)
    return ex/(1+ex)**2
 
def identity(x):
    return x
 
def identity_grad(x):
    return 1
    
import random
random.seed( 99 )

np.random.seed(seed=99)

'''
#First create the data.
n=2000
X=np.linspace(0,3*np.pi,num=n)
X.shape=(n,1)
y=10.*X
#We make a neural net with 2 hidden layers, 20 neurons in each, using logistic activation
#functions.
param=((1,0,0),(20, expit, logistic_grad),(20, expit, logistic_grad),(1,identity, identity_grad))
#Set learning rate.
rates=[0.005]
predictions=[]
for rate in rates:
    mlp=MLP(X,y,param)
    mlp.train(X, y, 40, learning_rate=rate)
    predictions.append([rate,mlp.predict(X)])


plt.plot(X,y, label='Sine', linewidth=2, color='black')
for data in predictions:
    plt.plot(X,data[1],label="Learning Rate: "+str(data[0]),linewidth=4)
plt.legend()
plt.show()

'''

#First create the data.
n=500
'''
x1 = np.random.uniform(-1., 1.,500)
x2 = np.random.uniform(-1., 1.,500) 
x3 = np.random.uniform(-1., 1.,500) 
X = np.vstack((x1,x2,x3)).T
y = 5*x1 - x3 + x2 + np.random.randn( 500)*0.5 
y = y.reshape(-1,1)


'''
x1 = np.random.uniform(-1., 1.,n)
x2 = np.random.uniform(-1., 1.,n) 
x3 = np.random.uniform(-1., 1.,n) 
x4 = np.random.uniform(-1., 1.,n) 
x5 = np.random.uniform(-1., 1.,n) 
x1.shape=(n,1)
x2.shape=(n,1)
x3.shape=(n,1)
x4.shape=(n,1)
x5.shape=(n,1)
y = (3*x1 + x3)**2 + x1 + 3*x2 + (x4 + x5)**2 + np.random.randn( n).reshape(-1,1)*0.1
X = np.hstack((x1,x2,x3,x4,x5))

'''

x1 = np.random.uniform(-1., 1.,500)
x2 = np.random.uniform(-1., 1.,500) 
y = 3*x1**2 + 3*x2**2 + np.random.randn( 500)*0.1
X = np.vstack((x1,x2)).T
y = y.reshape(-1,1)
'''
'''
x1 = np.random.uniform(1., 2.,500)
x2 = np.random.uniform(-1., 1.,500) 
y = 3*x1*x2 + np.random.randn( 500)*0.1
X = np.vstack((x1,x2)).T
y = y.reshape(-1,1)
'''
#We make a neural net with 2 hidden layers, 20 neurons in each, using logistic activation
#functions.
param=((5,0,0),(20, expit, logistic_grad),(1,identity, identity_grad))

nnfe=NNFE(X,y,param)
output, mi = nnfe.train()

#XY = np.hstack((output,y))
#nnfe._func_MI_Reg(XY)
'''
mlp=DFE(X,y,param)
print mlp._func_MI_Reg(np.hstack((mlp.predict(X), y)), split = [1,1])
'''




