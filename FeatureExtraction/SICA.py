from scipy.optimize import minimize
import copy
import numpy as np
from scipy.optimize import differential_evolution
from sklearn.neighbors.kde import KernelDensity
import time


#--------------------------------------------------------------------------
# compute canonical information analysis. Find two directions on which projections
# of two variables have largest mutual information-----dependency
#--------------------------------------------------------------------------
class CIA:
    
    # to store results for each component
    class Results:
        def __init__(self):
            self.x_dir = 0
            self.y_dir = 0
            self.x_proj = 0
            self.y_proj =0
            self.mi = 0
    
    def __init__(self, my_chain, my_split, num_components, optimizer = 'L-BFGS-B'):
        self.num_components = num_components
        self.my_chain = my_chain
        self.dim_x = my_split[0]
        self.dim_y = my_split[1]
        self.lenChain = my_chain.shape[0]
        self.dimChain = my_chain.shape[1]
        self.bounds = [(-1,1)]*self.dimChain # each weight is between 0 and 1
        self.results = [0]*num_components
        self.current_component = 0
        self.optimizer = optimizer 
        for i in range(0, num_components):
            self.results[i] = self.Results()
    
    # lagrange multiplier (equality constraints since new projections should hava 0 mutual information with previous ones)
    def ortho_fun(self, new):
        xy = self.unitize(copy.deepcopy(new))
        dirX = np.array([xy[0:self.dim_x]]).T
        dirY = np.array([xy[self.dim_x:self.dimChain]]).T
        projX = np.dot(self.my_chain[:,0:self.dim_x],dirX)
        projY = np.dot(self.my_chain[:,self.dim_x:self.dimChain], dirY)
        reg = 0.
        for i in range(0, self.current_component):
            projXX = np.hstack((self.results[i].x_proj, projX))
            projYY = np.hstack((self.results[i].y_proj, projY))
            projXY = np.hstack((self.results[i].y_proj, projX))
            projYX = np.hstack((self.results[i].x_proj, projY))
            reg = reg + max(self.func_MI(projXX,[1,1]),0)# + max(self.func_MI(projYY, [1,1]),0) + max(self.func_MI(projYX, [1,1]),0) + max(self.func_MI(projXY, [1,1]),0)
        #return 1/(4.*self.current_component)*reg
        return reg

    # compute mutual information
    def func(self,x):
        if self.dim_x >1:
            dirX = x[0:self.dim_x].reshape(-1,1)
            projX = np.dot(self.my_chain[:,0:self.dim_x],dirX)
        else:
            projX = self.my_chain[:,0:self.dim_x]
        if self.dim_y > 1:
            dirY = x[self.dim_x:self.dimChain].reshape(-1,1)
            projY = np.dot(self.my_chain[:,self.dim_x:self.dimChain],dirY)
        else:
            projY = self.my_chain[:,self.dim_x:self.dimChain]
        projXY = np.hstack((projX,projY))
        #plt.figure()
        #plt.plot(projXY[:,0],projXY[:,1],'.')
        
        if(self.current_component == 0):
            ortho_constraints = 0
        else:
            ortho_constraints = self.ortho_fun(x)
            return -self.func_MI(projXY,[1,1]) + ortho_constraints
        
        return -self.func_MI(projXY, [1,1])
    
    # unitize the direction to keep magnitude 1
    def unitize(self,aPopulation):
        population = copy.deepcopy(aPopulation)
        sum = 0.
        for j in range(0, self.dim_x):
            sum = sum + abs(population[j])**2
        for j in range(0, self.dim_x):
            population[j] = population[j]/np.sqrt(sum)
        sum = 0.
        for j in range(self.dim_x, self.dimChain):
            sum = sum + abs(population[j])**2
        for j in range(self.dim_x, self.dimChain):
            population[j] = population[j]/np.sqrt(sum)
        return population

    # main function to search for CIA projecting directions
    def opt(self):
        for i in range(0, self.num_components):
            start_time = time.time()
            self.current_component = i
            if self.optimizer == 'Diff-Evol':
                output = differential_evolution(self.func, self.bounds, maxiter = 100,popsize = 15,disp=True,polish =True)
            else:
                print 'initializing...'
                initialV = differential_evolution(self.func, self.bounds, maxiter = 10,popsize = 20,disp=True,polish =True)
                                
                                
                cons = ({'type': 'eq',
                         'fun': lambda x: (np.dot(x[0:self.dim_x],x[0:self.dim_x].reshape(-1,1)) - 1)**2 + (np.dot(x[self.dim_x:self.dimChain],x[self.dim_x:self.dimChain].reshape(-1,1)) - 1)**2})
                         #'jac': lambda x: np.hstack((4*(np.dot(x[0:self.dim_x],x[0:self.dim_x].reshape(-1,1)) - 1)*x[0:self.dim_x],4*(np.dot(x[self.dim_x:self.dimChain],x[self.dim_x:self.dimChain].reshape(-1,1)) - 1)*x[self.dim_x:self.dimChain]))})
                print 'looking for optima'              
                output = minimize(self.func,x0 = initialV.x, constraints = cons, method=self.optimizer, bounds=self.bounds, options={'disp': True, 'maxiter': 100})
            result_dir = self.unitize(output.x)
            end_time = time.time()
            time_lapse = end_time - start_time
            #result_dir = output.x
            self.results[i].x_dir = copy.deepcopy(result_dir)[0:self.dim_x]
            self.results[i].y_dir = copy.deepcopy(result_dir)[self.dim_x:self.dimChain]
            self.results[i].x_proj = np.dot(self.my_chain[:,0:self.dim_x],np.array([self.results[i].x_dir]).T)
            self.results[i].y_proj = np.dot(self.my_chain[:,self.dim_x: self.dimChain],np.array([self.results[i].y_dir]).T)
            self.results[i].mi = self.func_MI(np.hstack((self.results[i].x_proj, self.results[i].y_proj)),[1,1])
            print self.results[i].x_proj.shape
            print 'CIA number ', i
            print 'x dir:'
            print self.results[i].x_dir
            print 'y dir:'
            print self.results[i].y_dir
            print 'MI: '
            print self.results[i].mi
            print 'time: '
            print time_lapse
        
        return self.results
    def func_deriv(self, a):
        deriv =  -self.func_MI_Jac(a, self.my_chain[:,0:self.dim_x], self.my_chain[:,self.dim_x:self.dimChain])
        print deriv
        return deriv
                
    def GKernel(self,x):
        return np.exp(-0.5*(x**2))
    def sumSingleGKernel(self,x, x_vec,h):
        if np.size(x)>1:
            sumGK = np.sum(self.GKernel((x - x_vec)/h),axis = 0)
        else:
            sumGK = np.sum(self.GKernel((x - x_vec)/h))
        return sumGK
    def sumJointGKernel(self,x, x_vec, y, y_vec, h_11, h_22):
        if np.size(x)>1:
            sumGK = np.sum(self.GKernel((x - x_vec)/h_11)*self.GKernel((y - y_vec)/h_22),axis = 0)
        else:
            sumGK = np.sum(self.GKernel((x - x_vec)/h_11)*self.GKernel((y - y_vec)/h_22))
        return sumGK
    '''
    def sumSingleGKernelJac(self,x, x_vec, x_row,h):
        exp_vec = self.GKernel((x - x_vec)/h)*(x - x_vec)/h**2
        print exp_vec.shape
        exp_mat = np.dot(exp_vec,x_row)
        #sumGK = np.sum(exp_mat, axis = 0) 
        return exp_mat

    def sumJointGKernelJac(self,x, x_vec, y, y_vec, x_row, h_11, h_22,h):
        exp_vec = self.GKernel((x - x_vec)/h_11)*self.GKernel((y - y_vec)/h_22)*(x - x_vec)/h**2
        exp_mat = np.dot(exp_vec,x_row)
        #sumGK = np.sum(exp_mat, axis = 0)        
        return exp_mat   
    '''    
    def sumSingleGKernelJac(self,x,index,a, x_vec, x_row,h):
        '''
        sumGK = 0
        for i in range(np.size(x_vec)):
            sumGK = sumGK + self.GKernel((x - x_vec[i])/h)/(h**2)*np.dot(a, np.dot((x_row[index]- x_row[i]).reshape(-1,1), np.array([x_row[index]- x_row[i]])))
        return -sumGK
        '''
        EXP = self.GKernel((x - x_vec)/h)
        #print 'EXP', EXP.shape
        term_2 = x_row[index] - x_row
        #print 'term_2', term_2.shape
        term_1 = np.dot(a, term_2.T).reshape(-1,1)
        #print 'term_1', term_1.shape
        sumGK = 1./(h**2)*np.sum(EXP *term_1*term_2, axis = 0)
        #print 'sumGK', sumGK.shape
        return -sumGK

    def sumJointGKernelJac(self,x,index,a, x_vec, y,y_vec, x_row, h_11, h_22,h):
        '''
        sumGK = 0
        for i in range(np.size(x_vec)):
            sumGK = sumGK + self.GKernel((x - x_vec[i])/h_11)*self.GKernel((y - y_vec[i])/h_22)/(h**2)*np.dot(a, np.dot((x_row[index]- x_row[i]).reshape(-1,1), np.array([x_row[index]- x_row[i]])))     
        return -sumGK 
        '''
        EXP = self.GKernel((x - x_vec)/h_11)*self.GKernel((y - y_vec)/h_22)
        term_2 = x_row[index] - x_row
        term_1 = np.dot(a, term_2.T).reshape(-1,1)
        sumGK = 1./(h**2)*np.sum(EXP *term_1*term_2, axis = 0)
        return -sumGK
    
    def func_MI_Jac(self,a, x, y):
        dim_x = x.shape[1]
        dim_y = y.shape[1]
        dimChain = x.shape[0]
        proj_x = np.dot(x,a[0:dim_x].reshape(-1,1))
        proj_y = np.dot(y,a[dim_x:(dim_y+dim_x)].reshape(-1,1))
        std_1 = np.std(proj_x)
        std_2 = np.std(proj_y)
        h_1 = 1.06*std_1*dimChain**(-0.2)
        h_2 = 1.06*std_2*dimChain**(-0.2)
        h_11 = std_1*dimChain**(-1./6)
        h_22 = std_2*dimChain**(-1./6)
        # partial derivative wrt a[0:dim_x]
        sum_x = 0
        for i in range(dimChain):
            sum_x = sum_x + self.sumJointGKernelJac(proj_x[i],i,a[0:dim_x], proj_x, proj_y[i], proj_y, x, h_11, h_22,h_11)/self.sumJointGKernel(proj_x[i], proj_x, proj_y[i], proj_y, h_11, h_22) - self.sumSingleGKernelJac(proj_x[i],i,a[0:dim_x], proj_x, x,h_1)/self.sumSingleGKernel(proj_x[i], proj_x,h_1)
       
        # partial derivative wrt a[dim_x:(dim_y+dim_x)]
        sum_y = 0
        for i in range(dimChain):
            sum_y = sum_y + self.sumJointGKernelJac(proj_x[i],i,a[dim_x:(dim_y+dim_x)], proj_x, proj_y[i], proj_y, y, h_11, h_22,h_22)/self.sumJointGKernel(proj_x[i], proj_x, proj_y[i], proj_y, h_11, h_22) - self.sumSingleGKernelJac(proj_y[i],i,a[dim_x:(dim_y+dim_x)], proj_y, y,h_2)/self.sumSingleGKernel(proj_y[i], proj_y,h_2)
        
        #sum_mi = np.hstack((sum_x,sum_y))/dimChain
        if(self.current_component !=0):
            print 'cross derivatives'
            for j in range(self.current_component): 
                                 
                for i in range(dimChain):
                    sum_x = sum_x - self.sumJointGKernelJac(proj_x[i],i,a[0:dim_x], proj_x, self.results[j].y_proj[i], self.results[j].y_proj, x, h_11, h_22,h_11)/self.sumJointGKernel(proj_x[i], proj_x, self.results[j].y_proj[i], self.results[j].y_proj, h_11, h_22) - self.sumSingleGKernelJac(proj_x[i],i,a[0:dim_x], proj_x, x,h_1)/self.sumSingleGKernel(proj_x[i], proj_x,h_1)
                for i in range(dimChain):
                    sum_x = sum_x - self.sumJointGKernelJac(proj_x[i],i,a[0:dim_x], proj_x, self.results[j].x_proj[i], self.results[j].x_proj, x, h_11, h_22,h_11)/self.sumJointGKernel(proj_x[i], proj_x, self.results[j].x_proj[i], self.results[j].x_proj, h_11, h_22) - self.sumSingleGKernelJac(proj_x[i],i,a[0:dim_x], proj_x, x,h_1)/self.sumSingleGKernel(proj_x[i], proj_x,h_1)
                  
                # partial derivative wrt a[dim_x:(dim_y+dim_x)]
                
                for i in range(dimChain):
                    sum_y = sum_y - self.sumJointGKernelJac(self.results[j].x_proj[i],i,a[dim_x:(dim_y+dim_x)], self.results[j].x_proj, proj_y[i], proj_y, y, h_11, h_22,h_22)/self.sumJointGKernel(self.results[j].x_proj[i], self.results[j].x_proj, proj_y[i], proj_y, h_11, h_22) - self.sumSingleGKernelJac(proj_y[i],i,a[dim_x:(dim_y+dim_x)], proj_y, y,h_2)/self.sumSingleGKernel(proj_y[i], proj_y,h_2) 
                for i in range(dimChain):
                    sum_y = sum_y - self.sumJointGKernelJac(self.results[j].y_proj[i],i,a[dim_x:(dim_y+dim_x)], self.results[j].y_proj, proj_y[i], proj_y, y, h_11, h_22,h_22)/self.sumJointGKernel(self.results[j].y_proj[i], self.results[j].y_proj, proj_y[i], proj_y, h_11, h_22) - self.sumSingleGKernelJac(proj_y[i],i,a[dim_x:(dim_y+dim_x)], proj_y, y,h_2)/self.sumSingleGKernel(proj_y[i], proj_y,h_2) 
              
        sum_mi = np.hstack((sum_x,sum_y))/dimChain
        return sum_mi   
    
    def func_MI(self, projXY, split = [1,1]):
        dimChain = projXY.shape[0]
        x_proj = projXY[:,0]
        y_proj = projXY[:,1]
        proj_x = x_proj
        proj_y = y_proj
        std_1 = np.std(proj_x)
        std_2 = np.std(proj_y)
        h_1 = 1.06*std_1*dimChain**(-0.2)
        h_2 = 1.06*std_2*dimChain**(-0.2)
        h_11 = std_1*dimChain**(-1./6)
        h_22 = std_2*dimChain**(-1./6)  
        sum_mi = 0
        for i in range(dimChain):
            sum_mi = sum_mi + np.sum(np.log(self.sumJointGKernel(proj_x[i], proj_x.reshape(-1,1), proj_y[i], proj_y.reshape(-1,1), h_11, h_22)) - np.log(self.sumSingleGKernel(proj_x[i], proj_x.reshape(-1,1),h_1)) - np.log(self.sumSingleGKernel(proj_y[i], proj_y.reshape(-1,1),h_2)))
        sum_mi = sum_mi + dimChain* np.log(h_1*h_2*dimChain/(h_11*h_22))       
        return sum_mi/dimChain
        
    def func_MI_tree(self, projXY, split = [1,1]):
        lenChain = projXY.shape[0]
        x_proj = projXY[:,0]
        y_proj = projXY[:,1]
        proj_x = x_proj
        proj_y = y_proj
        std_1 = np.std(proj_x)
        std_2 = np.std(proj_y)
        h_1 = 1.06*std_1*lenChain**(-0.2)
        h_2 = 1.06*std_2*lenChain**(-0.2)
        h_11 = std_1*lenChain**(-1./6)
        h_22 = std_2*lenChain**(-1./6)   
        h = np.sqrt(h_11*h_22)
        sum_mi = np.sum(self.sumKDE(projXY, projXY, h) - self.sumKDE(proj_x.reshape(-1,1), proj_x.reshape(-1,1),h_1) - self.sumKDE(proj_y.reshape(-1,1), proj_y.reshape(-1,1),h_2))
        return sum_mi/lenChain
        
    def sumKDE(self,test_x, train_x,h):
        kde = KernelDensity(kernel = 'gaussian', bandwidth = h).fit(train_x)
        return kde.score_samples(test_x)