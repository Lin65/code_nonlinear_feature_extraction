# -*- coding: utf-8 -*-
"""
Created on Wed Oct 26 02:27:10 2016

@author: xiaolin
"""

import numpy as np
from sklearn.cluster import KMeans
from sklearn.neighbors import radius_neighbors_graph
from scipy.special import gamma
from scipy.special import hermite
import time
import matplotlib.pyplot as plt
np.random.seed(seed=70)

'''
def entropy(X, p, n_cls):
    n_smps = X.shape[0]
    std_X = np.std(X)
    # h -> bandwidth for kernel estimator
    h = 1.06*std_X*n_smps**(-0.2) 
    sigma = h*np.sqrt(2)
    
    kmeans = KMeans(n_clusters = n_cls, random_state=0).fit()
    clusters = []
    # store data as a list of tuples (center, data)
    for i in range(n_cls):
        clusters.append((kmeans.cluster_centers_[i][0], X[np.where(kmeans.labels_== i)[0]]))  
        
    # compute exponential term within each cluster
    exp_cls = []
    for center, smps in clusters:  
        one_cls = []
        for i in range(p):
            one_cls.append(1./gamma(i+1)*((smps - center)/sigma)**i)
        exp_cls.append(one_cls)    
    
    # compute radius of each cluster
    radius_cls = []
    for center, smps in clusters:
        radius_cls.append(max(smps - center))
        
    # compute entropy
    sum_ent = 0
    for i in range(n_cls):
        center  = n_cls[i][0]
        smps = n_cls[i][1]
        for x in smps:
            for j in range(n_cls):
                if x - clusters[j][0] <= radius_cls[j] + 3*h:
                    # compute sum in this cluster
                    vcnp.exp(((x - clusters[j][0])/sigma)**2)*sum(exp_cls[j]*H((x - clusters[j][0])/sigma))
'''            
        
    
    
'''
X = np.array([[1], [1], [3],
              [5], [8], [7]])
kmeans = KMeans(n_clusters=2, random_state=0).fit(X)

clusters = []
n_cls = 2

# store data as a list of tuples (center, data)
for i in range(n_cls):
    clusters.append((kmeans.cluster_centers_[i][0], X[np.where(kmeans.labels_== i)[0]]))  
    
'''
# try to see how many terms are needed? choose  P?


# 
print hermite(0)(1)
print hermite(1)(1)
print hermite(2)(1)

'''
# ----------- test Hermite expansion ---------------#
t = 2.
s = 1.
c = 1.6
h = 1.
p = 10

truth  = np.exp(-((t-s)/h)**2)

t_1 = np.exp(-((t-c)/h)**2)
t_2 =0

for i in range(p):
    t_2 = t_2 + (1./gamma(i+1))*(((s-c)/h)**i)*(hermite(i)((t-c)/h))
    
print truth
print t_1*t_2
'''

# ------------ test fast Gauss Transform on calculating sum for one point ------------- #


'''
# clustering
t2_start = time.time()

n_cls = int(np.sqrt(n_smps)/3+1)
kmeans = KMeans(n_clusters = n_cls, max_iter = 5, random_state=0).fit(X)
clusters = []
# store data as a list of tuples (center, data)

#plt.figure()
for i in range(n_cls):
    center = kmeans.cluster_centers_[i][0]
    data = X[np.where(kmeans.labels_== i)[0]]
    radius = np.max(abs(data - center))
    clusters.append((center,data,radius)) 
    #clusters.append((center,data))
    #plt.hist(X[np.where(kmeans.labels_== i)[0]],5)

# compute Bj for each cluster
P = 6
B = []
for i in range(n_cls):
    Bj = np.zeros((P))
    for j in range(P):
        Bj[j] = np.sum(1./gamma(j+1)*((clusters[i][1] - clusters[i][0])/sigma)**j)
    B.append(Bj)

# evaluate test_point at each cluster
sum_approx = 0
for i in range(n_cls):
    dist = abs(test_point - clusters[i][0]) -  (clusters[i][2] + 3*sigma)
    #print dist
    if dist <0:
        sum_cls = 0
        for j in range(P):
            sum_cls = sum_cls + B[i][j]*hermite(j)((test_point - clusters[i][0])/sigma)*np.exp(-((test_point-clusters[i][0])/sigma)**2)
    
        sum_approx = sum_approx + sum_cls
    

t2_end = time.time()

print sum_approx, '   ', t2_end-t2_start
'''
#------------------------------ def FastGaussTransform ----------------------------------#

def FastGaussTrans_1d(X,sigma,test_point):
    # n_cls clusters are generated
    n_cls = int(np.sqrt(len(X))/3)
    t_Kmeans = time.time()
    kmeans = KMeans(n_clusters = n_cls, max_iter = 5, random_state=0, n_jobs=1).fit(X)
    t_Kmeans_end = time.time()
    print "time consumed by Kmeans: ", t_Kmeans_end - t_Kmeans
    # store (center, data, radius) for each cluster in a list
    # analyze each cluster
    t_tuple = time.time()
    clusters = []
    for i in range(n_cls):
        center = kmeans.cluster_centers_[i][0]
        data = X[np.where(kmeans.labels_== i)[0]]
        radius = np.max(abs(data - center))
        clusters.append((center,data,radius,radius/sigma))
    t_tuple_end = time.time()
    print "time consumed by tuples ", t_tuple_end - t_tuple

    
    # compute P for each cluster
    t_P = time.time()
    # decide P
    P= [0]*n_cls
    for i in range(n_cls):
        ratio = clusters[i][3]
        if(ratio>=1.):
            P[i]= 4
        elif(ratio >= 0.7):
            P[i]= 4
        elif(ratio>= 0.5):
            P[i] =4
        elif(ratio >= 0.3):
            P[i]=4
        elif(ratio >= 0.2):
            P[i]= 4
        else:
            P[i]= 4

    
    t_P_end = time.time()
    print "time consumed by P ", t_P_end - t_P
    
    # compute Bj for each cluster
    t_Bj = time.time()
    B = []
    for i in range(n_cls):
        Bj = np.zeros((P[i]))
        for j in range(P[i]):
            Bj[j] = np.sum(1./gamma(j+1)*((clusters[i][1] - clusters[i][0])/sigma)**j)
        B.append(Bj)
    t_Bj_end = time.time()
    print "time consumed by Bj ", t_Bj_end - t_Bj
    # evaluate test_point at each cluster
    t_eval = time.time()
    sum_approx = 0
    
    #for k in range(len(test_point)):
    #    for i in range(n_cls):
    #        # only evaluate in nearby clusters
    #        if abs(test_point[k,0] - clusters[i][0]) < (clusters[i][2] + 3*sigma):                
    #            for j in range(P[i]):
    #                sum_approx = sum_approx + B[i][j]*hermite(j)((test_point[k,0] - clusters[i][0])/sigma)*np.exp(-((test_point[k,0]-clusters[i][0])/sigma)**2)    
  
    for i in range(n_cls):
        # only evaluate in nearby clusters
        if True:                
            for j in range(P[i]):
                sum_approx = sum_approx + np.sum(B[i][j]*hermite(j)((test_point[:,0] - clusters[i][0])/sigma)*np.exp(-((test_point[:,0]-clusters[i][0])/sigma)**2) )   
      
   
    t_eval_end = time.time()
    print "time consumed by final evaluation ", t_eval_end - t_eval
    return sum_approx/len(X)
    



# generate data

n_smps = 4000
#X = np.hstack((np.random.normal(0., 0.7, n_smps),np.random.normal(3.5, 0.5, n_smps))).reshape(-1,1)
X = np.random.normal(0.,2000,n_smps).reshape(-1,1)
#\ln\left(\sigma\sqrt{2\,\pi\,e}\right) 
#import matplotlib.pyplot as plt
#plt.hist(X,200) 


test_point = X#np.array([[-.2]])

std_X = np.std(X)
ent_truth = np.log(std_X*np.sqrt(2*np.pi*np.e))
# h -> bandwidth for kernel estimator
h = 1.06*std_X*n_smps**(-0.2) 
sigma = h*np.sqrt(2)

t1_start = time.time()
truth_1 = 0

for i in range(n_smps):
    p = 1./(h*n_smps)*np.sum(1./(np.sqrt(2*np.pi))*np.exp(-0.5*((test_point[:,0] - X[i,0])/h)**2))
    if p!=0.:
        truth_1 = truth_1 + np.log(p)
t1_end = time.time()
    
print 'truth:  ', -truth_1/n_smps, '  ', t1_end-t1_start


t2_start = time.time()
print FastGaussTrans_1d(X,sigma,test_point)
t2_end = time.time()
print t2_end - t2_start

from fastkde import fastKDE
t3_start = time.time()
pdf, axe = fastKDE.pdf(X[:,0])
#pdf_bin = (pdf[1:]+pdf[0:-1])/2.
pdf_bin = pdf
non_zero_index  = np.where(pdf_bin!= 0.)[0]
pdf_bin = pdf_bin[non_zero_index]
#axe_bin = axe[1:] - axe[0:-1]
axe_bin = axe
#ent_approx = -np.sum((axe_bin[non_zero_index])*pdf_bin*np.log(pdf_bin))
ent_approx = -(axe[1] - axe[0])*np.sum(pdf_bin*np.log(pdf_bin))

t3_end = time.time()
print "fastKDE time: " ,t3_end - t3_start
print "ent truth: ", ent_truth, "  ent approx: ", ent_approx
'''
import scipy.spatial.distance as dist

def FastGaussTrans_2d(X,sigma_1,sigma_2,test_point):
    # n_cls clusters are generated
    n_cls = int(np.sqrt(len(X))/3)
    t_Kmeans = time.time()
    kmeans = KMeans(n_clusters = n_cls, max_iter = 3, random_state=0, n_jobs=1).fit(X)
    t_Kmeans_end = time.time()
    print "time consumed by Kmeans: ", t_Kmeans_end - t_Kmeans
    # store (center, data, radius) for each cluster in a list
    # analyze each cluster
    t_tuple = time.time()
    clusters = []
    for i in range(n_cls):
        center = kmeans.cluster_centers_[i]
        data = X[np.where(kmeans.labels_== i)[0]]
        radius = np.max(np.sqrt((data[:,0] - center[0])**2 + (data[:,1] - center[1])**2))
        clusters.append((center,data,radius))
    t_tuple_end = time.time()
    print "time consumed by tuples ", t_tuple_end - t_tuple

    
    # compute P for each cluster
    t_P = time.time()
    # decide P
    P= [(6,6)]*n_cls
    
    t_P_end = time.time()
    print "time consumed by P ", t_P_end - t_P
    
    # compute Bj for each cluster
    t_Bj = time.time()
    B = []
    for i in range(n_cls):
        Bj = np.zeros((P[i]))
        for r in range(P[i][0]):
            for c in range(P[i][1]):
                Bj[r,c] = np.sum((1./gamma(r+1)*((clusters[i][1][:,0:1] - clusters[i][0][0])/sigma_1)**r)*(1./gamma(c+1)*((clusters[i][1][:,1:2] - clusters[i][0][1])/sigma_2)**c) )
        B.append(Bj)
    t_Bj_end = time.time()
    print "time consumed by Bj ", t_Bj_end - t_Bj
    # evaluate test_point at each cluster
    t_eval = time.time()
    sum_approx = 0
       
    for i in range(n_cls):
        # only evaluate in nearby clusters
        if True:                
            for r in range(P[i][0]):
                for c in range(P[i][1]):
                    sum_approx = sum_approx + B[i][r,c]*np.sum((hermite(r)((test_point[:,0:1] - clusters[i][0][0])/sigma_1)*np.exp(-((test_point[:,0:1]-clusters[i][0][0])/sigma_1)**2))*(hermite(c)((test_point[:,1:2] - clusters[i][0][1])/sigma_2)*np.exp(-((test_point[:,1:2]-clusters[i][0][1])/sigma_2)**2)))  
      
   
    t_eval_end = time.time()
    print "time consumed by final evaluation ", t_eval_end - t_eval
    
    #plt.figure()
    #for i in range(n_cls):
    #    plt.plot(X[np.where(kmeans.labels_== i)[0]][:,0],X[np.where(kmeans.labels_== i)[0]][:,1],'.')
    return sum_approx/len(X)




n_smps = 16000
#X = np.random.multivariate_normal([0,1], [[100, 0], [0, 100]], n_smps)
# uniform
x1 = np.random.uniform(0,1000, n_smps)
x2 = np.random.uniform(0,10,n_smps)

X = np.vstack((x1,x2)).T

test_point = X#np.array([[.2,0.3]])

std_x = np.std(X[:,0:1])
std_y = np.std(X[:,1:2])

#\frac{1}{2}\ln\{(2\pi e)^{N} \det(\Sigma)\}
ent_truth_2d = 0.5*np.log((2*np.pi*np.e)**2*std_x**2*std_y**2)
        
h_xx = std_x*n_smps**(-1./6)
h_yy = std_y*n_smps**(-1./6)

sigma_1 = h_xx*np.sqrt(2)
sigma_2 = h_yy*np.sqrt(2)

t1_start = time.time()
truth_2 = 0

for i in range(n_smps):
    p = 1./(h_xx*h_yy*n_smps)*np.sum(1./np.sqrt(2*np.pi)*np.exp(-((test_point[:,0] - X[i,0])/sigma_1)**2)*np.exp(-((test_point[:,1] - X[i,1])/sigma_2)**2))
    if p!=0.:
        truth_2 = truth_2 + np.log(p)
t1_end = time.time()


    
print 'truth:  ', -truth_2/len(X), '  ', t1_end-t1_start


print FastGaussTrans_2d(X,sigma_1,sigma_2,test_point)

t3_start = time.time()
#Do the self-consistent density estimate
myPDF,axes = fastKDE.pdf(X[:,0],X[:,1])
v1,v2 = axes
area_bin = (v1[1] - v1[0])*(v2[1]-v2[0])
ent_approx_2d = -area_bin*np.sum(myPDF*np.log(myPDF+ 1e-16))
#(p1[1:]+p1[0:-1] +
t3_end = time.time()
print "fastKDE time: " ,t3_end - t3_start
print "entropy truth: ", ent_truth_2d, "  entropy approx: ", ent_approx_2d
#Extract the axes from the axis list
import pylab as PP
#Plot contours of the PDF should be a set of concentric ellipsoids centered on
#(0.1, -300) Comparitively, the y axis range should be tiny and the x axis range
#should be large
PP.contour(v1,v2,myPDF)
PP.show()

t4_start = time.time()
pOfYGivenX,axes = fastKDE.conditional(x1,x2)
t4_end = time.time()

print "conditional time: ", t4_end - t4_start
'''