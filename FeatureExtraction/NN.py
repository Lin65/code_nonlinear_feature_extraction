# -*- coding: utf-8 -*-
"""
Created on Tue Oct  4 18:11:50 2016

@author: xiaolin
"""
""" 
 Nonlinear mapping using neural network, in this version, gradient decent is used
"""

import numpy as np
import random
from pyswarm import pso
random.seed( 99 )

class MLP(object):
    def __init__(self, X, y, parameters):
        #Input data
        self.X=X
        #Output data
        self.y=y
        #Expect parameters to be a tuple of the form:
        #    ((n_input,0,0), (n_hidden_layer_1, f_1, f_1'), ...,
        #     (n_hidden_layer_k, f_k, f_k'), (n_output, f_o, f_o'))
        self.n_layers = len(parameters)
        #Counts number of neurons without bias neurons in each layer.
        self.sizes = [layer[0] for layer in parameters]
        #Activation functions for each layer.
        self.fs =[layer[1] for layer in parameters]
        #Derivatives of activation functions for each layer.
        self.fgrads = [layer[2] for layer in parameters]
        self.build_network()
 
    def build_network(self):
        #List of weight matrices taking the output of one layer to the input of the next.
        self.weights=[]
        #Bias vector for each layer.
        self.biases=[]
        #Input vector for each layer.
        self.inputs=[]
        #Output vector for each layer.
        self.outputs=[]
        #Vector of delta at each layer.
        self.delta=[]
        #We initialise the weights randomly, and fill the other vectors with 1s.
        for layer in range(self.n_layers-1):
            n = self.sizes[layer]
            m = self.sizes[layer+1]
            self.weights.append(np.random.normal(0,1, (m,n)))
            self.biases.append(np.random.normal(0,1,(m,1)))
            self.inputs.append(np.zeros((n,1)))
            self.outputs.append(np.zeros((n,1)))
            self.delta.append(np.zeros((n,1)))
        #There are only n-1 weight matrices, so we do the last case separately.
        n = self.sizes[-1]
        self.inputs.append(np.zeros((n,1)))
        self.outputs.append(np.zeros((n,1)))
        self.delta.append(np.zeros((n,1)))
 
    def feedforward(self, x):
        #Propagates the input from the input layer to the output layer.
        #k=len(x)
        #x.shape=(k,1)
        self.inputs[0]=x
        self.outputs[0]=x
        for i in range(1,self.n_layers):
            self.inputs[i]=self.weights[i-1].dot(self.outputs[i-1])+self.biases[i-1]
            self.outputs[i]=self.fs[i](self.inputs[i])
        return self.outputs[-1]
 
    def update_weights(self,x,y):
        #Update the weight matrices for each layer based on a single input x and target y.
        # the output of MLP
        output = self.feedforward(x)
        self.delta[-1]=self.fgrads[-1](self.outputs[-1])*(output-y)
 
        n=self.n_layers-2
        for i in xrange(n,0,-1):
            self.delta[i] = self.fgrads[i](self.inputs[i])*self.weights[i].T.dot(self.delta[i+1])
            self.weights[i] = self.weights[i]-self.learning_rate*np.outer(self.delta[i+1],self.outputs[i])
            self.biases[i] = self.biases[i] - self.learning_rate*self.delta[i+1]
        self.weights[0] = self.weights[0]-self.learning_rate*np.outer(self.delta[1],self.outputs[0])
        self.biases[0] = self.biases[0] - self.learning_rate*self.delta[1] 
        
    def train(self, X, Y, n_iter, learning_rate=0.05):
        #Updates the weights after comparing each input in X with y
        #repeats this process n_iter times.
        self.X = X
        self.y= Y
        self.learning_rate=learning_rate
        n=self.X.shape[0]
        for aIter in range(n_iter):
            #We shuffle the order in which we go through the inputs on each iter.
            index=list(range(n))
            np.random.shuffle(index)
            for row in index:
                x=self.X[row:row+1,:]
                y=self.y[row:row+1,:]
                self.update_weights(x.T,y.T)
   
    def predict_x(self, x):
        return self.feedforward(x)
 
    def predict(self, X):
        n = len(X)
        m = self.sizes[-1]
        results = np.ones((n,m))
        for i in range(len(X)):
            results[i,:] = self.feedforward(X[i:i+1,:].T).reshape((m))
        return results
#expit is a fast way to compute logistic using precomputed exp.

class DFE(MLP):
    
    def __init__(self,X, y, parameters):
        MLP.__init__(self, X, y, parameters)
        self.weights_batch = []
        self.biases_batch = []
        for layer in range(self.n_layers-1):
            n = self.sizes[layer]
            m = self.sizes[layer+1]
            self.weights_batch.append(np.random.normal(0,1,(m,n)))
            self.biases_batch.append(np.random.normal(0,1,(m,1)))
    
    def update_weights_batch(self,X,y):
        
        output = self.predict(X)
        delta_MI = self._func_MI_grad(output) 
        self._zero_weights()
        for i in range(self.X.shape[0]):
            self._compute_weights_batch(self.X[i,:],delta_MI[i:i+1,:])

    
    def feedforward_batch(self,X):
        #Propagates the input from the input layer to the output layer.
      
        self.inputs[0]=X
        self.outputs[0]=X
        for i in range(1,self.n_layers):
            self.inputs[i]=(self.weights[i-1].dot(self.outputs[i-1].T)+self.biases[i-1]).T
            self.outputs[i]=self.fs[i](self.inputs[i])
        
        return self.outputs[-1]
 

    def _zero_weights(self):
        for i in range(len(self.weights)):
            self.weights_batch[i][:,:] = 0.0
            self.biases_batch[i][:] = 0.
            
    def _compute_weights_batch(self, x, delta_MI): 

        #output = self.feedforward(x)
        self.delta[-1] = self.fgrads[-1](self.outputs[-1])*(delta_MI.T)
 
        n=self.n_layers-2
        for i in xrange(n,0,-1):
            self.delta[i] = self.fgrads[i](self.inputs[i])*self.weights[i].T.dot(self.delta[i+1])
            self.weights_batch[i] += (np.outer(self.delta[i+1],self.outputs[i])+0.0001*(self.weights[i]))
            self.biases_batch[i] += ((self.delta[i+1])+0.0001*(self.biases[i]))
        self.weights_batch[0] += (np.outer(self.delta[1],self.outputs[0])+0.0001*(self.weights[0]))
        self.biases_batch[0] += (self.delta[1] +0.0001*(self.biases[0]) )       
        
        
    def _update_weights(self):   
        for i in range(len(self.weights)):
            #print self.weights_batch[i]
            self.weights[i] = self.weights[i]-self.learning_rate*self.weights_batch[i]
            self.biases[i] = self.biases[i]-self.learning_rate*self.biases_batch[i]      
    
    def train(self,n_iter,learning_rate=0.05):
        #Updates the weights after comparing each input in X with y
        #repeats this process n_iter times.
        '''
        self.learning_rate=learning_rate
        MI_XY = 0
        for repeat in range(n_iter):
            #output = self.predict(self.X)
            print 'iteration: ', repeat
            self.update_weights_batch(self.X, self.y)
            self._update_weights()
            XY = np.hstack((self.predict(self.X),self.y))
            pre_MI = MI_XY
            MI_XY = self._func_MI_Reg(XY)
            if MI_XY< pre_MI:
                self.learning_rate = self.learning_rate/2.
           
            import matplotlib.pyplot as plt
            plt.plot(self.predict(self.X), self.y,'.')
            
            print "mutual information of iteration ", repeat, " is ", MI_XY
           
        ''' 
        '''
        self.bounds_ = [(-1,1)]*( np.size(self.weights[0]) + np.size(self.weights[1]) + np.size(self.biases[0]) +np.size(self.biases[1]))
        output = differential_evolution(self._func, self.bounds_, maxiter = 100, popsize = 20,disp=True,polish =True)
        '''
        
        lb = [-1]*( np.size(self.weights[0]) + np.size(self.weights[1])  + np.size(self.biases[0]) +np.size(self.biases[1]))
        ub = [1]*( np.size(self.weights[0]) + np.size(self.weights[1])  + np.size(self.biases[0]) +np.size(self.biases[1]))
        xopt, fopt = pso(self._func, lb, ub, debug = True)
        print 'mi: ' , fopt
    
    def _func(self, w):
        pre_w = 0     
        for layer in range(self.n_layers-1):
            n = self.sizes[layer]
            m = self.sizes[layer+1]
            self.weights[layer] =  w[pre_w:(pre_w + m*n)].reshape(m,n)            
            self.biases[layer] = w[(pre_w + m*n):(pre_w + m*n + m)].reshape(m,1)
            pre_w = pre_w + m*n + m
            
        output = self.predict(self.X)
        XY = np.hstack((output,self.y))
        
        return -self._func_MI_Reg(XY)
        
        
        
        
        
        
    def _func_MI_grad(self, x):
        
        """ derivative of the cost function
        
        Parameters
        ----------
        a: vector
            direction of a projection
        
        Returns
        -------
        return the value of derivative of cost function on the direction a
        
        """ 
        n_samples = len(x)
        projX = x

        std_x = np.std(projX)
        std_y = np.std(self.y)
        
        h_x = 1.06*std_x*n_samples**(-0.2)
        h_xx = std_x*n_samples**(-1./6)
        h_yy = std_y*n_samples**(-1./6)
        

        # derivative of I(s,Y)
        grad_MI = np.zeros((n_samples,1))
        for k in range(n_samples):
            sum_xy = 0
            for i in range(n_samples):
                sum_xy = sum_xy + self._jointPDFJac(k,projX[i],i, projX, self.y[i], self.y, x, h_xx, h_yy)/self._sumJointGKernel(projX[i], projX,self.y[i], self.y, h_xx, h_yy) \
                - self._singlePDFJac(k,projX[i], i, projX, x, h_x)/self._sumSingleGKernel(projX[i], projX,h_x)
            sum_xy = sum_xy/n_samples
            grad_MI[k:(k+1),:] = sum_xy

            
     
        '''
        # derivative of \sum I(s,si)   
        sum_X = 0
        if(self.current_component_ > 0):
            for j in range(0,self.current_component_):
                # derivative of I(s,si)
                sum_x = 0
                si = self.projections_[:,j]
                std_si = np.std(si)
                h_si = std_si*n_samples**(-1./6)
                for i in range(n_samples):
                    sum_x = sum_x + self._jointPDFJac(projX[i],i, projX, si[i], si, self.X, h_xx, h_si)/self._sumJointGKernel(projX[i], projX, si[i], si, h_xx, h_si) \
                    - self._singlePDFJac(projX[i], i, projX, self.X, h_x)/self._sumSingleGKernel(projX[i], projX,h_x)
                sum_x = sum_x/n_samples
            sum_X = sum_X + sum_x
        ''' 
        return -grad_MI #- self.a * sum_X        

    def _jointPDFJac(self,k,x,index, x_vec, y,y_vec, X, h_11, h_22):        
       
        """ derivative of joint pdf
        
        Parameters
        ----------
        a: vector
            direction of a projection
        x: float
            a point of the projection, on which the derivative is evaluated
        index: int
            the index of x in x_vec
        x_vec: vector
            projection of X on the direction a
        y: float
            the target value corresponding to X[index] 
        X: array, (n_samples, n_features)
            original dataset (excluding target)
        h_11: float
            bandwidth of kernel estimator on x_vec
        h_22: float
            bandwith of kernel estimator on y_vec
        
        Returns
        -------
        return the value of derivative of joint pdf wrt a at (x,y)
        
        """  
        if k == index:
            sum_xy = 0
            for j in range(0, len(x_vec)):
                if j!= k:
                    sum_xy = sum_xy + self._GKernelJac((x_vec[index] - x_vec[j])/h_11)/h_11*self._GKernel((y -y_vec[j])/h_22)
        
            sum_xy_I =  sum_xy/(len(x_vec)*h_11*h_22)
            return sum_xy_I
        else:
            sum_xy = self._GKernelJac((x_vec[index] - x_vec[k])/h_11)*(-1.)/h_11*self._GKernel((y -y_vec[k])/h_22)
            sum_xy_II = sum_xy/(len(x_vec)*h_11*h_22)
            return sum_xy_II
            
        
        
        
    def _singlePDFJac(self,k, x, index, x_vec, X, h):        
       
        """ derivative of marginal pdf
        
        Parameters
        ----------
        a: vector
            direction of a projection
        x: float
            a point of the projection, on which the derivative is evaluated
        index: int
            the index of x in x_vec
        x_vec: vector
            projection of X on the direction a
        X: array, (n_samples, n_features)
            original dataset (excluding target)
        h: float
            bandwidth of kernel estimator on x_vec

        Returns
        -------
        return the value of derivative of marginal pdf wrt a at x
        
        """     
        if k == index:
            sum_x = 0
            for j in range(0, len(x_vec)):
                if j != k:
                    sum_x = sum_x + self._GKernelJac((x_vec[index] - x_vec[j])/h)/h
        
            sum_x_I = sum_x/(len(x_vec)*h)
            return sum_x_I
        else:
            sum_x = self._GKernelJac((x_vec[index] - x_vec[k])/h)*(-1.)/h
            sum_x_II =  sum_x/(len(x_vec)*h)
            return sum_x_II
        
     
    def _GKernelJac(self,u):
        
        """ derivative of Gaussian kernel
        
        Parameters
        ----------
        u: float
            input of Gaussian kernel
        
        Returns
        -------
        return the value of derivative of Gaussian kernel at u
        
        """
        
        return -u/np.sqrt(2*np.pi)*np.exp(-0.5*(u.astype(float)**2))
        
  
    
    def _func_MI_Reg(self, XY, split = [1,1]):
        """ kernel estimation of mutual information, only works for two dimensional problems. The second column is from a continous variable
    
        Parameters
        ----------
        XY: array,  [n_samples, 2]
            each column represent a variable
    
        Returns
        -------
        return the mutual information between X and Y
    
        """                        
        n_samples = XY.shape[0]
        proj_x = XY[:,0]
        proj_y = XY[:,1]
        std_1 = np.std(proj_x)
        std_2 = np.std(proj_y)
        h_1 = 1.06*std_1*n_samples**(-0.2)
        #print 1.06*std_1*n_samples**(-0.2)
        h_2 = 1.06*std_2*n_samples**(-0.2)
        #print 1.06*std_2*n_samples**(-0.2)
        h_11 = std_1*n_samples**(-1./6)
        #print std_1*n_samples**(-1./6)
        h_22 = std_2*n_samples**(-1./6)  
        #print std_2*n_samples**(-1./6)  
        sum_mi = 0
        for i in range(n_samples):
            sum_mi = sum_mi + np.log(self._sumJointGKernel(proj_x[i], proj_x, proj_y[i], proj_y, h_11, h_22)) - \
            np.log(self._sumSingleGKernel(proj_x[i], proj_x.reshape(-1,1),h_1)) - \
            np.log(self._sumSingleGKernel(proj_y[i], proj_y.reshape(-1,1),h_2))
              
        return sum_mi/n_samples
    
    def _GKernel(self,x):
        
        """ Gaussian kernel
    
        Parameters
        ----------
        x: scalar or vector
    
        Returns
        -------
        return corresponding scalar or vector
    
        """ 
        return 1./np.sqrt(2*np.pi)*np.exp(-0.5*(x.astype(float)**2))
        
        
    def _sumSingleGKernel(self,x, x_vec,h):
        
        """ kernel density estimation for single variable
    
        Parameters
        ----------
        x: float
            point that need to be evaluated
        x_vec: vector, (, n_samples)
            samples that construct the kernel estimator
        h: float
            bandwidth of kernel estimator
    
        Returns
        -------
        return the density of x
    
        """         
        sumGK = 1./(len(x_vec)*h)*np.sum(self._GKernel((x - x_vec)/h))
        return sumGK
        
        
    def _sumJointGKernel(self,x, x_vec, y, y_vec, h_11, h_22):
        
        """ kernel density estimation for joint variable
    
        Parameters
        ----------
        x: float
            (x,y) is the point that need to be evaluated
        x_vec: vector, (, n_samples)
            (x_vec, y_vec) are samples that construct the kernel estimator
        y: float
            (x,y) is the point that need to be evaluated
        y_vec: vector, (, n_samples)
            (x_vec, y_vec) are samples that construct the kernel estimator
        h_11: float
            bandwidth of kernel estimator for x
        h_22: float
            bandwidth of kernel estimator for y
        
        Returns
        -------
        return the mutual information between X and Y
        
        """ 
        sumGK = 1./(len(x_vec)*h_11*h_22)*np.sum(self._GKernel((x - x_vec)/h_11)*self._GKernel((y - y_vec)/h_22))
        return sumGK
    
    
    
    
    
    
    
    
    
    
    
    
