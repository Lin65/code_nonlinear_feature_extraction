# -*- coding: utf-8 -*-
"""
Created on Mon Oct 17 16:06:33 2016

@author: xiaolin
"""
import numpy as np
from NNFeatureExtraction import NFE
from scipy.special import expit

def tanh(x):
    return np.tanh(x)
def tanh_grad(x):
    return 1.0 - np.tanh(x)**2.0

def logistic(x):
    return 1.0/(1+np.exp(-x))
 
def logistic_grad(x):
    ex=expit(-x)
    return ex/(1+ex)**2
 
def identity(x):
    return x
 
def identity_grad(x):
    return 1
    
import random
random.seed( 99 )

np.random.seed(seed=99)

n = 500

x1 = np.random.uniform(-1., 1.,n)
x2 = np.random.uniform(-1., 1.,n) 
x3 = np.random.uniform(-1., 1.,n) 
x4 = np.random.uniform(-1., 1.,n) 
x5 = np.random.uniform(-1., 1.,n) 
x1.shape=(n,1)
x2.shape=(n,1)
x3.shape=(n,1)
x4.shape=(n,1)
x5.shape=(n,1)
y = (3*x1 + x3)**2 + x1 + 3*x2 + (x4 + x5)**2 + np.random.randn( n).reshape(-1,1)*0.1
X = np.hstack((x1,x2,x3,x4,x5))
y = y.T[0]
'''
x1 = np.random.uniform(1., 3.,500)
x2 = np.random.uniform(-1., 1.,500) 
y = 3*x1**2 + 3*x2**2 + np.random.randn( 500)*0.1
X = np.vstack((x1,x2)).T

'''
param=((5,0,0),(10, expit, logistic_grad),(1,identity, identity_grad))

nfe = NFE( n_components=5, a = 1, b = 0.01, optimizer = 'PSO', task ='regression')
nfe.fit(X,y, param)

