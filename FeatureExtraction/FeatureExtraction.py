from scipy.optimize import minimize
import copy
import numpy as np
from scipy.optimize import differential_evolution,fmin_l_bfgs_b
from pyswarm import pso
#from sklearn.neighbors.kde import KernelDensity
import time

np.random.seed(seed=99)
class SFE(object):
    """Supervised feature extraction (SFE)
    
    Supervised linear dimensionality reduction by selecting projections that maximize the dependence
    on the target at the same time minimize the dependence within projections. Mutual information is
    used as a metric for dependence, and is estimated via kernel estimation. 
    
    s = arg max I(Y;s) - a \sum I(s, s_i)  
    

    Parameters
    ----------
    n_components: int or string
        Number of components to keep.
        if n_components == 'min_dependency', process will end once \sum I(s, s_i) >= b. Default value is 'min_dependency'
        
    b: float or string
        The maximum inner pairwise dependence that can be torlerated.  b is used only if n_components == 'min_dependency'.Defaul value is 0.01 
        
    a: float or string 
        The coefficient of the regularizer. Defaul value is 1.
        if a == 'average', a = 1/Ns, Ns is the number of current extracted features
        
    optimizer: string, optional
        Default value is 'L-BFGS-B'. Other optimization methods from scipy can also be used.
    
    task: string {'regression', 'classification'}
        indicate whether it is a regression problem or classification. Default is 'regression'
        
    Attributes
    ----------
    components_ : array, [n_components, n_features]
        Principal axes in feature space, representing the projecting directions.
        
    projections_: array, [n_samples, n_components]
        new features. projections_ = X * components_.T
        
    mi_: vector, (,n_components)
        record mutual information between Y and each projection.
    
    n_components: int
        The estimated number of components. Relevant when n_components is set to 'min_dependency'.
        
    
    Examples
    --------
    
    >>> import numpy as np
    >>> import SFE
    >>> X = np.array([[],[],[])
    >>> Y = np.array([])
    >>> sfe = SFE(n_components='min_dependency', a=1, b= 0.1, optimizer = 'L-BFGS-B', type = 'regression')
    >>> sfe.fit(X,Y)
    >>> sfe.n_components
    >>> sfe.components_
    >>> sfe.mi_
    
    """
    
    
    def __init__(self, n_components='min_dependency', a = 1, b = 0.01, optimizer = 'L-BFGS-B', task ='regression' ):
        self.n_components = n_components
        self.optimizer = optimizer
        self.a = a
        self.b = b
        self.type = task
        
 
    def fit(self, X, Y):
        """Fit the model with X,Y.
    
        Parameters
        ----------
        X: array-like, shape (n_samples, n_features)
            Training data, where n_samples is the number of samples and n_features is the number of features.
        Y: vector, shape (1, n_samples)    
    
        Returns
        -------
        self : object
            Return the instance itself
        """
        self._fit(X,Y)
        return self
        
  
    def _fit(self, X,Y):
        
        """Fit the model with X and Y, self.components_ is calculated
    
        Parameters
        ----------
        X: array-like, shape (n_samples, n_features)
            Training data, where n_samples is the number of samples and n_features is the number of features.
        Y: vector, shape ( , n_samples)    
    
        Returns
        -------
        No return value
    
        """ 
        self.X = X
        self.Y = Y
        # delete import attribute each time
        self.del_attr = []        
        
        n_samples, n_features = X.shape
        
        # initialization
        n_TBD = False 
        if self.n_components == 'min_dependency':
            self.n_components = n_features
            n_TBD = True
            
        self.components_ = np.zeros((self.n_components, n_features))
        self.projections_ = np.zeros((n_samples,self.n_components))
        self.mi_ = np.zeros(self.n_components)
        
        # restrict the weight of direction in ( -1, 1)
        self.bounds_ = [(-1,1)]*n_features
        for i in range(0, self.n_components):
            
            print 'Processing component %d'%i
            # calculate time elapse for each component
            start_time = time.time()
            
            self.current_component_ = i
            if self.optimizer == 'PSO':
                lb = [-1.]*n_features
                ub = [1.]*n_features
                xopt, fopt = pso(self._func, lb, ub, swarmsize =50, maxiter=100, minstep = 1e-8, minfunc=1e-8, debug = False)
                output = xopt
            elif self.optimizer == 'Deff_evol':
                output_all = differential_evolution(self._func, self.bounds_, maxiter = 50, popsize = 20,disp=False,polish =False)                
                output = output_all.x
            else:
                print 'initializing...'
                '''
                from sklearn.decomposition import PCA
                pca = PCA(n_components=1)
                pca.fit(self.X)
                initialV = pca.components_[0]
                '''
                #initialV = differential_evolution(self._func, self.bounds_, maxiter = 5, popsize = 10,disp=True,polish =False)
                #initialV.x  [1./n_features]*n_features
                # center data so that CCA can be applied
                if self.type == 'regression':
                    mean_X = np.mean(self.X,axis = 0) 
                    mean_Y = np.mean(self.Y)
                
                    post_X = self.X - mean_X
                    post_Y = self.Y - mean_Y
                
                    self.X = post_X
                    self.Y = post_Y
                
                    from sklearn.cross_decomposition import CCA
                    cca = CCA(n_components=1)
                    cca.fit(post_X,post_Y)
                    initialV = cca.x_weights_[:,0]
                    print initialV  
                else:
                    from sklearn.decomposition import PCA
                    pca = PCA(n_components=1)
                    pca.fit(self.X)
                    initialV = pca.components_[0,:]
                    print initialV
                cons = ({'type': 'eq',
                         'fun': lambda x: (np.dot(x,x.reshape(-1,1)) - 1),
                         'jac': lambda x: (2*x)})
                #jac = self._func_deriv,
                print 'looking for optima'              
                #output = minimize(self._func,x0 = initialV, constraints = cons, method=self.optimizer, jac = self._func_deriv, bounds=self.bounds_,options={'disp': True, 'maxls': 20, 'iprint': -1, 'gtol': 1e-05, 'eps': 1e-06, 'maxiter': 10, 'ftol': 2.220446049250313e-06, 'maxcor': 10, 'maxfun': 10})
                #output = optimize.fmin_bfgs(f = self._func,x0 = initialV,fprime = self._func_deriv,disp = True, retall = True)
                output = fmin_l_bfgs_b(self._func,x0 = initialV, fprime=self._func_deriv, args=(), approx_grad=0, bounds=self.bounds_, m=10, factr=10000000.0, pgtol=1e-05, epsilon=1e-05, iprint=10, maxfun=150, maxiter=150, disp=True, callback=None, maxls=20)
                output = output.x
            result_dir = self._unitize(output)
            result_dir[self.del_attr] = 0
            self.del_attr.append(np.argmax(abs(result_dir)))
            
            end_time = time.time()
            time_elapse = end_time - start_time
            print 'time elapse for component %d is %d s'%(i,time_elapse)

            self.components_[i,:] = result_dir
            

            self.projections_[:,i] = np.dot(X, result_dir.reshape(-1,1)).T[0]
            projXY = np.hstack((self.projections_[:,i].reshape(-1,1), Y.reshape(-1,1)))
            if self.type =='regression':
                self.mi_[i] = self._func_MI_Reg(projXY, [1,1]) 
            elif self.type == 'classification':
                self.mi_[i] = self._func_MI_Cla(projXY,[1,1])
            print 'mutual information: ',  self.mi_[i]  
            
            # inner dependency
            print 'mutual information between the new projection and previous projections are: '
            for j in range(0, self.current_component_):
                projXX = np.vstack((self.projections_[:,self.current_component_], self.projections_[:,j])).T
                print self._func_MI_Reg(projXX,[1,1])
            
            if (n_TBD == True) and (self.current_component_>0):
                # we need to calculate inner denpendency and stop the process if it is bigger than self.b
                for j in range(0, self.current_component_):
                    projXX = np.vstack((self.projections_[:,self.current_component_], self.projections_[:,j])).T
                    if self._func_MI_Reg(projXX,[1,1]) >= self.b: #0.2*self.mi_[self.current_component_]:
                        self.n_components = self.current_component_
                        self.components_ = self.components_[0:self.n_components, 0:n_features]
                        self.projections_ = self.projections_[0:n_samples, 0:self.n_components]
                        self.mi_ = self.mi_[:self.n_components]
                        return
                        
        
    def _func(self,x):
        
        """ calculate the cost function
    
        Parameters
        ----------
        x: a vector with shape ( ,n_features)
            represent the projecting direction       
    
        Returns
        -------
        return the value of the cost function
    
        """ 
        dirX = x.reshape(-1,1)

        # take out import attribut
        if(self.current_component_ == 0):        
            projX = np.dot(self.X,dirX)
        else:
            new_X = copy.deepcopy(self.X)
            new_X[:, self.del_attr] =0
            projX = np.dot(new_X, dirX)
            

        projXY = np.hstack((projX, self.Y.reshape(-1,1)))
        
        if self.type == 'regression':
            if(self.current_component_ == 0):
                indep_constraints = 0
            else:
                indep_constraints = max(0.,self._indep_fun(x))
                if self.a == 'average':
                    return -self._func_MI_Reg(projXY,[1,1]) + 1./self.current_component_ *indep_constraints
                else:
                    return -self._func_MI_Reg(projXY,[1,1]) + self.a *indep_constraints
        
            return -self._func_MI_Reg(projXY, [1,1])    
        
        elif self.type == 'classification':
            if(self.current_component_ == 0):
                indep_constraints = 0
            else:
                #indep_constraints = max(0.,self._indep_fun(x))
                if self.a == 'average':
                    return -self._func_MI_Cla(projXY,[1,1]) #+ 1/self.current_component_ *indep_constraints
                else:
                    return -self._func_MI_Cla(projXY,[1,1]) #+ self.a *indep_constraints
        
            return -self._func_MI_Cla(projXY, [1,1]) 
            
            
    def _indep_fun(self, x):
        
        """ lagrange multiplier (new projection should hava 0 mutual information with previous ones)
    
        Parameters
        ----------
        x: a vector with shape ( ,n_features)
            represent the projecting direction        
    
        Returns
        -------
        return the value of the cost function
    
        """ 
        dirX = x.reshape(-1,1)
        projX = np.dot(self.X,dirX)
        reg = 0.
        for i in range(0, self.current_component_):
            projXX = np.hstack((self.projections_[:,i].reshape(-1,1), projX))
            reg = reg + max(self._func_MI_Reg(projXX,[1,1]),0.)
    
        return reg


    def _unitize(self,x):
        
        """ unitize the direction to keep magnitude 1
    
        Parameters
        ----------
        x: a vector with shape ( ,n_features)
            represent the projecting direction        
    
        Returns
        -------
        return the unitized direction
    
        """         
        population = copy.deepcopy(x)
        sum = 0.
        for j in range(0, len(x)):
            sum = sum + abs(population[j])**2
        for j in range(0, len(x)):
            population[j] = population[j]/np.sqrt(sum)
        sum = 0.
        
        return population
        

    def _func_MI_Reg(self, XY, split = [1,1]):
        """ kernel estimation of mutual information, only works for two dimensional problems. The second column is from a continous variable
    
        Parameters
        ----------
        XY: array,  [n_samples, 2]
            each column represent a variable
    
        Returns
        -------
        return the mutual information between X and Y
    
        """         
        
        
        n_samples = XY.shape[0]
        proj_x = XY[:,0]
        proj_y = XY[:,1]
        std_1 = np.std(proj_x)
        std_2 = np.std(proj_y)
        h_1 = 1.06*std_1*n_samples**(-0.2)
        #print 1.06*std_1*n_samples**(-0.2)
        h_2 = 1.06*std_2*n_samples**(-0.2)
        #print 1.06*std_2*n_samples**(-0.2)
        h_11 = std_1*n_samples**(-1./6)
        #print std_1*n_samples**(-1./6)
        h_22 = std_2*n_samples**(-1./6)  
        #print std_2*n_samples**(-1./6)
        sum_mi = 0
        for i in range(n_samples):
            sum_mi = sum_mi + np.log(self._sumJointGKernel(proj_x[i], proj_x, proj_y[i], proj_y, h_11, h_22)) - \
            np.log(self._sumSingleGKernel(proj_x[i], proj_x.reshape(-1,1),h_1)) - \
            np.log(self._sumSingleGKernel(proj_y[i], proj_y.reshape(-1,1),h_2))
              
        return sum_mi/n_samples
  
            
    def _func_MI_Cla(self, XY, split = [1,1]):
        """ kernel estimation of mutual information, only works for two dimensional problems.The second column is from a discrete variable
    
        Parameters
        ----------
        XY: array,  [n_samples, 2]
            each column represent a variable
    
        Returns
        -------
        return the mutual information between X and Y
    
        """         

        n_samples = XY.shape[0]
        proj_x = XY[:,0]
        proj_y = XY[:,1]
        std_1 = np.std(proj_x)
        h_1 = 1.06*std_1*n_samples**(-0.2)
            
        # calculate H(proj_x)
        Ent_x = 0.
        for i in range(0,n_samples):
            Ent_x = Ent_x + np.log(self._sumSingleGKernel(proj_x[i], proj_x.reshape(-1,1),h_1))
        Ent_x = - Ent_x/n_samples
            
        # calculate \sum prop(yi)*H(proj_x|yi)
        sum_Ent = 0
        n_classes = np.unique(proj_y)
        for yi in n_classes:
            indices = np.where(proj_y == yi)[0]
            n_samples_sub = len(indices)
            # frequency of yi
            prop = n_samples_sub/float(n_samples)
            proj_x_sub = proj_x[indices]  
            std_sub = np.std(proj_x_sub)
            h_sub = 1.06*std_sub*n_samples_sub**(-0.2)
            if h_sub == 0.0:
                Ent_sub = 0.
            else:
                Ent_sub = 0.
                # calculate H(proj_x|yi)
                for i in range(0,n_samples_sub):
                    Ent_sub = Ent_sub + np.log(self._sumSingleGKernel(proj_x_sub[i], proj_x_sub.reshape(-1,1),h_sub))                
                Ent_sub = - Ent_sub/n_samples_sub
            sum_Ent = sum_Ent + prop*Ent_sub
            
        return Ent_x - sum_Ent   
            

    def _GKernel(self,x):
        
        """ Gaussian kernel
    
        Parameters
        ----------
        x: scalar or vector
    
        Returns
        -------
        return corresponding scalar or vector
    
        """ 
        return 1./np.sqrt(2*np.pi)*np.exp(-0.5*(x.astype(float)**2))
        
        
    def _sumSingleGKernel(self,x, x_vec,h):
        
        """ kernel density estimation for single variable
    
        Parameters
        ----------
        x: float
            point that need to be evaluated
        x_vec: vector, (, n_samples)
            samples that construct the kernel estimator
        h: float
            bandwidth of kernel estimator
    
        Returns
        -------
        return the density of x
    
        """         
        sumGK = 1./(len(x_vec)*h)*np.sum(self._GKernel((x - x_vec)/h))
        return sumGK
        
        
    def _sumJointGKernel(self,x, x_vec, y, y_vec, h_11, h_22):
        
        """ kernel density estimation for joint variable
    
        Parameters
        ----------
        x: float
            (x,y) is the point that need to be evaluated
        x_vec: vector, (, n_samples)
            (x_vec, y_vec) are samples that construct the kernel estimator
        y: float
            (x,y) is the point that need to be evaluated
        y_vec: vector, (, n_samples)
            (x_vec, y_vec) are samples that construct the kernel estimator
        h_11: float
            bandwidth of kernel estimator for x
        h_22: float
            bandwidth of kernel estimator for y
        
        Returns
        -------
        return the mutual information between X and Y
        
        """ 
        sumGK = 1./(len(x_vec)*h_11*h_22)*np.sum(self._GKernel((x - x_vec)/h_11)*self._GKernel((y - y_vec)/h_22))
        return sumGK

    def transform(self,X_new):
        
        """ project new data on obtained directions
        
        Parameters
        ----------
        X_new: array, has the same number of features as self.X
        
        Returns
        -------
        return the projections of X_new
        
        """
        return np.dot(X_new, self.components_.T) 


    def _func_deriv(self, a):
        
        """ derivative of the cost function
        
        Parameters
        ----------
        a: vector
            direction of a projection
        
        Returns
        -------
        return the value of derivative of cost function on the direction a
        
        """        
        # minus -- minimization
        if self.type == 'regression':
            deriv =  -self._func_MI_Jac_Reg(a)
        elif self.type == 'classification':
            deriv = -self._func_MI_Jac_Cla(a)
        return deriv
                
    def _func_MI_Jac_Reg(self,a):
        
        """ derivative of the cost function
        
        Parameters
        ----------
        a: vector
            direction of a projection
        
        Returns
        -------
        return the value of derivative of cost function on the direction a
        
        """ 
        n_samples = self.X.shape[0]
        dirX = a.reshape(-1,1)
        projX = np.dot(self.X,dirX)

        std_x = np.std(projX)
        std_y = np.std(self.Y)
        
        h_x = 1.06*std_x*n_samples**(-0.2)
        h_xx = std_x*n_samples**(-1./6)
        h_yy = std_y*n_samples**(-1./6)
        
        # derivative of I(s,Y)
        sum_xy = 0
        for i in range(n_samples):
            sum_xy = sum_xy + self._jointPDFJac(projX[i],i, projX, self.Y[i], self.Y, self.X, h_xx, h_yy)/self._sumJointGKernel(projX[i], projX,self.Y[i], self.Y, h_xx, h_yy) \
            - self._singlePDFJac(projX[i], i, projX, self.X, h_x)/self._sumSingleGKernel(projX[i], projX,h_x)
        sum_xy = sum_xy/n_samples
        
        # derivative of \sum I(s,si)   
        sum_X = a*0 
        if(self.current_component_ > 0):
            for j in range(0,self.current_component_):
                # derivative of I(s,si)
                sum_x = 0
                si = self.projections_[:,j]
                std_si = np.std(si)
                h_si = std_si*n_samples**(-1./6)
                for i in range(n_samples):
                    sum_x = sum_x + self._jointPDFJac(projX[i],i, projX, si[i], si, self.X, h_xx, h_si)/self._sumJointGKernel(projX[i], projX, si[i], si, h_xx, h_si) \
                    - self._singlePDFJac(projX[i], i, projX, self.X, h_x)/self._sumSingleGKernel(projX[i], projX,h_x)
                sum_x = sum_x/n_samples
                sum_X = sum_X + sum_x
        #print type(sum_xy),sum_xy
        #print type(self.a),self.a
        #print type(sum_X),sum_X
        
        if (self.a == 'average') and (self.current_component_>0):
            return sum_xy - 1./self.current_component_*sum_X
        elif((self.a == 'average') and (self.current_component_==0)):
            return sum_xy
        
        return sum_xy - self.a * sum_X
 

    def _func_MI_Jac_Cla(self,a):
        
        """ derivative of the cost function
        
        Parameters
        ----------
        a: vector
            direction of a projection
        
        Returns
        -------
        return the value of derivative of cost function on the direction a
        
        """ 
        n_samples = self.X.shape[0]
        dirX = a.reshape(-1,1)
        projX = np.dot(self.X,dirX)

        std_x = np.std(projX)        
        h_x = 1.06*std_x*n_samples**(-0.2)
        h_xx = std_x*n_samples**(-1./6)
        
        # derivative of H(s)
        sum_s = 0
        for i in range(n_samples):
            sum_s = sum_s - self._singlePDFJac(projX[i], i, projX, self.X, h_x)/self._sumSingleGKernel(projX[i], projX,h_x)
        sum_s = sum_s/n_samples
        
        # derivative of H(s|Y=yi)
        sum_Ent = 0
        n_classes = np.unique(self.Y)
        for yi in n_classes:
            indices = np.where(self.Y == yi)[0]
            n_samples_sub = len(indices)
            # frequency of yi
            prop = n_samples_sub/float(n_samples)
            
            proj_x_sub = projX[indices]  
            std_sub = np.std(proj_x_sub)
            h_sub = 1.06*std_sub*n_samples_sub**(-0.2)
            
            Ent_sub = 0
            # calculate derivative of H(proj_x|Y=yi)
            for i in range(0,n_samples_sub):
                Ent_sub = Ent_sub + self._singlePDFJac(proj_x_sub[i], i, proj_x_sub, self.X[indices], h_sub)/self._sumSingleGKernel(proj_x_sub[i], proj_x_sub,h_sub)
                
            Ent_sub = - Ent_sub/n_samples_sub
            sum_Ent = sum_Ent + prop*Ent_sub
        
        sum_xy = sum_s - sum_Ent
        # derivative of \sum I(s,si)   
        sum_X = a*0
        if(self.current_component_ > 0):
            for j in range(0,self.current_component_):
                # derivative of I(s,si)
                sum_x = 0
                si = self.projections_[:,j]
                std_si = np.std(si)
                h_si = std_si*n_samples**(-1./6)
                for i in range(n_samples):
                    sum_x = sum_x + self._jointPDFJac(projX[i],i, projX, si[i], si, self.X, h_xx, h_si)/self._sumJointGKernel(projX[i], projX, si[i], si, h_xx, h_si) \
                    - self._singlePDFJac(projX[i], i, projX, self.X, h_x)/self._sumSingleGKernel(projX[i], projX,h_x)
                sum_x = sum_x/n_samples
                sum_X = sum_X + sum_x
                
        if (self.a == 'average') and (self.current_component_>0):
            return sum_xy - 1./self.current_component_*sum_X
        elif((self.a == 'average') and (self.current_component_==0)):
            return sum_xy
            
        return sum_xy - self.a * sum_X
        

    def _jointPDFJac(self,x,index, x_vec, y,y_vec, X, h_11, h_22):        
       
        """ derivative of joint pdf
        
        Parameters
        ----------
        a: vector
            direction of a projection
        x: float
            a point of the projection, on which the derivative is evaluated
        index: int
            the index of x in x_vec
        x_vec: vector
            projection of X on the direction a
        y: float
            the target value corresponding to X[index] 
        X: array, (n_samples, n_features)
            original dataset (excluding target)
        h_11: float
            bandwidth of kernel estimator on x_vec
        h_22: float
            bandwith of kernel estimator on y_vec
        
        Returns
        -------
        return the value of derivative of joint pdf wrt a at (x,y)
        
        """  
        sum_xy = 0
        for j in range(0, len(x_vec)):
            sum_xy = sum_xy + self._GKernelJac((x_vec[index] - x_vec[j])/h_11)*(X[index] - X[j])/h_11*self._GKernel((y -y_vec[j])/h_22)
        
        return sum_xy/(len(x_vec)*h_11*h_22)
        
        
    def _singlePDFJac(self, x, index, x_vec, X, h):        
       
        """ derivative of marginal pdf
        
        Parameters
        ----------
        a: vector
            direction of a projection
        x: float
            a point of the projection, on which the derivative is evaluated
        index: int
            the index of x in x_vec
        x_vec: vector
            projection of X on the direction a
        X: array, (n_samples, n_features)
            original dataset (excluding target)
        h: float
            bandwidth of kernel estimator on x_vec

        Returns
        -------
        return the value of derivative of marginal pdf wrt a at x
        
        """        
        sum_x = 0
        for j in range(0, len(x_vec)):
            sum_x = sum_x + self._GKernelJac((x_vec[index] - x_vec[j])/h)*(X[index] - X[j])/h
        
        return sum_x/(len(x_vec)*h)
     
     
    def _GKernelJac(self,u):
        
        """ derivative of Gaussian kernel
        
        Parameters
        ----------
        u: float
            input of Gaussian kernel
        
        Returns
        -------
        return the value of derivative of Gaussian kernel at u
        
        """
        
        return -u/np.sqrt(2*np.pi)*np.exp(-0.5*(u.astype(float)**2))
        
  